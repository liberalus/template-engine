<?php

/**
 * @author  Evaldas Kazlauskas (kazlauskas.evaldas@gmail.com)
 *
 * @license http://www.gnu.org/licenses/old-licenses/gpl-2.0-standalone.html
 */

declare(strict_types = 1);

namespace Liberalu\Template\Core;

use Liberalu\Template\Parser\ParserInterface;
use Liberalu\Template\Parser\Scope;

/**
 * Class Parser
 */
class Parser
{
    private const PRIORITY = [
        Scope::BLOCK,
        Scope::VARIABLE,
    ];

    /** @var ParserInterface[] */
    private $parsers = [];

    /**
     * @param ParserInterface $parser
     */
    public function addParser(ParserInterface $parser): void
    {
        $this->parsers[$parser->getScope()][$parser->getName()] = $parser;
    }


    /**
     * @param string $content
     * @param array  $variables
     *
     * @return string
     */
    public function parse(string $content, array $variables = []): string
    {
        $parsers = $this->getParsers();
        foreach (self::PRIORITY as $priority) {
            if (isset($parsers[$priority])) {
                /** @var ParserInterface $parser */
                foreach ($parsers[$priority] as $parser) {
                    $content = $parser->parse($content, $variables);
                }
            }
        }

        return $content;
    }

    /**
     * @return array
     */
    private function getParsers(): array
    {
        return $this->parsers;
    }
}
