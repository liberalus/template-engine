<?php

/**
 * @author  Evaldas Kazlauskas (kazlauskas.evaldas@gmail.com)
 *
 * @license http://www.gnu.org/licenses/old-licenses/gpl-2.0-standalone.html
 */

declare(strict_types = 1);

namespace Liberalu\Template\Core;

use Symfony\Component\DependencyInjection\ContainerBuilder;
use Symfony\Component\Config\FileLocator;
use Symfony\Component\DependencyInjection\Loader\YamlFileLoader;

/**
 * Class Container
 *
 * @codeCoverageIgnore
 */
class Container
{
    /**
     * @return ContainerBuilder
     *
     * @throws \Exception
     */
    public function build(): ContainerBuilder
    {
        $container = new ContainerBuilder();
        $this->loadServicesFromConfigFile($container);
        $this->passParserTags($container);

        return $container;
    }

    /**
     * @param ContainerBuilder $container
     */
    private function loadServicesFromConfigFile(ContainerBuilder $container): void
    {
        $loader = new YamlFileLoader($container, new FileLocator(__DIR__));
        $loader->load('service.yaml');
    }

    /**
     * @param ContainerBuilder $container
     *
     * @throws \Exception
     */
    private function passParserTags(ContainerBuilder $container): void
    {
        /** @var Parser $parser */
        $parser = $container->get('core.parser');

        $taggedServices = $container->findTaggedServiceIds('template.parser');

        /**
         * @var string $serviceId
         * @var array  $tags
         */
        foreach ($taggedServices as $serviceId => $tags) {
            $parser->addParser($container->get($serviceId));
            unset($tags);
        }
    }
}
