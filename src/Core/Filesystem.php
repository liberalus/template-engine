<?php

/**
 * @author  Evaldas Kazlauskas (kazlauskas.evaldas@gmail.com)
 *
 * @license http://www.gnu.org/licenses/old-licenses/gpl-2.0-standalone.html
 */

declare(strict_types = 1);

namespace Liberalu\Template\Core;

use Symfony\Component\Filesystem\Filesystem as SymfonyFilesystem;

/**
 * Class Filesystem
 */
class Filesystem extends SymfonyFilesystem
{

    private const TEMPLATE_EXTENSION = 'tmpl';

    /**
     * @param string $filePath
     *
     * @return bool
     */
    public function isFileValid(string $filePath): bool
    {
        return $this->exists($filePath)
            && is_file($filePath)
            && (self::TEMPLATE_EXTENSION === pathinfo($filePath, \PATHINFO_EXTENSION))
            ;
    }

    /**
     * @param string $filePath
     *
     * @return string
     */
    public function getContent(string $filePath): string
    {
        return file_get_contents($filePath);
    }
}
