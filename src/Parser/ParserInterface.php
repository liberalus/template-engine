<?php

/**
 * @author  Evaldas Kazlauskas (kazlauskas.evaldas@gmail.com)
 *
 * @license http://www.gnu.org/licenses/old-licenses/gpl-2.0-standalone.html
 */

declare(strict_types = 1);

namespace Liberalu\Template\Parser;

/**
 * Interface ParserInterface
 */
interface ParserInterface
{
    /**
     * @return string
     */
    public function getName(): string;

    /**
     * @return string
     */
    public function getScope(): string;

    /**
     * @param string $content
     * @param array  $globalVariables
     *
     * @return string
     */
    public function parse(string $content, array $globalVariables = []): string;
}
