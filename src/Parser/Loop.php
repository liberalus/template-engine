<?php

/**
 * @author  Evaldas Kazlauskas (kazlauskas.evaldas@gmail.com)
 *
 * @license http://www.gnu.org/licenses/old-licenses/gpl-2.0-standalone.html
 */

declare(strict_types = 1);

namespace Liberalu\Template\Parser;

use Liberalu\Template\Core\Parser;

/**
 * Class Loop
 */
class Loop implements ParserInterface
{
    /** @var Parser */
    private $parser;

    /**
     * @param Parser $parser
     */
    public function __construct(Parser $parser)
    {
        $this->parser = $parser;
    }


    /**
     * @return string
     */
    public function getName(): string
    {
        return 'loop';
    }

    /**
     * @return string
     */
    public function getScope(): string
    {
        return Scope::BLOCK;
    }

    /**
     * @param string $content
     * @param array  $globalVariables
     *
     * @return string
     */
    public function parse(string $content, array $globalVariables = []): string
    {
        $pattern = '/{{#each(.*?)}}[\r\n]+(.*){{\/each}}/ms';

        $matches = [];
        preg_match_all($pattern, $content, $matches, PREG_SET_ORDER, 0);

        foreach ($matches as $match) {
            $loopVariable = $this->findVariable(trim($match[1]), $globalVariables);
            $replaceText = $this->loopBlock($match[2], $loopVariable, $globalVariables);
            $content = preg_replace($pattern, $replaceText, $content);
        }

        return $content;
    }

    /**
     * @param string $loopVariableName
     * @param array  $variables
     *
     * @return array
     */
    private function findVariable(string $loopVariableName, array $variables): array
    {
        if (!isset($variables[$loopVariableName]) || !\is_array($variables[$loopVariableName])) {
            throw new \InvalidArgumentException('Loop variable is wrong');
        }

        return $variables[$loopVariableName];
    }

    /**
     * @param string $content
     * @param array  $loopVariables
     * @param array  $globalVariables
     *
     * @return string
     */
    private function loopBlock(string $content, array $loopVariables, array $globalVariables): string
    {
        $parsedContent = '';
        foreach ($loopVariables as $key => $variables) {
            $variables = \array_merge(
                $variables,
                $globalVariables,
                $this->addSpecialVariables($key, $loopVariables)
            );
            $parsedContent .= $this->parser->parse($content, $variables);
        }

        return $parsedContent;
    }

    /**
     * @param mixed $key
     * @param array $loopVariables
     *
     * @return array
     */
    private function addSpecialVariables($key, array $loopVariables): array
    {
        $specVariables = [];

        \end($loopVariables);
        $specVariables['@last'] = $key === \key($loopVariables);

        return $specVariables;
    }
}
