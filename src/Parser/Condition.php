<?php

/**
 * @author  Evaldas Kazlauskas (kazlauskas.evaldas@gmail.com)
 *
 * @license http://www.gnu.org/licenses/old-licenses/gpl-2.0-standalone.html
 */

declare(strict_types = 1);

namespace Liberalu\Template\Parser;

use Liberalu\Template\Core\Parser;

/**
 * Class Condition
 */
class Condition implements ParserInterface
{
    /** @var Parser */
    private $parser;

    /**
     * @param Parser $parser
     */
    public function __construct(Parser $parser)
    {
        $this->parser = $parser;
    }


    /**
     * @return string
     */
    public function getName(): string
    {
        return 'condition';
    }

    /**
     * @return string
     */
    public function getScope(): string
    {
        return Scope::BLOCK;
    }

    /**
     * @param string $content
     * @param array  $globalVariables
     *
     * @return string
     */
    public function parse(string $content, array $globalVariables = []): string
    {
        $pattern = '/{{#unless(.*?)}}[\r\n]*(.*){{else}}[\r\n]*(.*){{\/unless}}/ms';

        $matches = [];
        preg_match($pattern, $content, $matches);

        if ($matches) {
            $replaceText = $this->elseIfConditions($matches, $globalVariables);
            $content = preg_replace($pattern, $replaceText, $content);
        }

        return $content;
    }

    /**
     * @param array $matches
     * @param array $globalVariables
     *
     * @return mixed
     */
    private function elseIfConditions(array $matches, array $globalVariables)
    {
        $condition = $this->isConditionTrue($matches, $globalVariables);

        $block = $matches[2];
        if ($condition) {
            $block = $matches[3];
        }

        return $this->parser->parse($block);
    }

    /**
     * @param array $matches
     * @param array $globalVariables
     *
     * @return bool
     */
    private function isConditionTrue(array $matches, array $globalVariables): bool
    {
        if (!isset($matches[1]) || !isset($globalVariables[trim($matches[1])])) {
            throw new \InvalidArgumentException('Condition doesn\'t extst');
        }

        return (bool) $globalVariables[trim($matches[1])];
    }
}
