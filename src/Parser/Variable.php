<?php

/**
 * @author  Evaldas Kazlauskas (kazlauskas.evaldas@gmail.com)
 *
 * @license http://www.gnu.org/licenses/old-licenses/gpl-2.0-standalone.html
 */

declare(strict_types = 1);

namespace Liberalu\Template\Parser;

/**
 * Class Variable
 */
class Variable implements ParserInterface
{
    /**
     * @return string
     */
    public function getName(): string
    {
        return 'variable';
    }

    /**
     * @return string
     */
    public function getScope(): string
    {
        return Scope::VARIABLE;
    }

    /**
     * @param string $content
     * @param array  $globalVariables
     *
     * @return string
     */
    public function parse(string $content, array $globalVariables = []): string
    {

        $pattern = '/{{(.*?)}}/';

        $matches = [];
        preg_match_all($pattern, $content, $matches, PREG_SET_ORDER, 0);

        foreach ($matches as $match) {
            if (isset($globalVariables[$match[1]])) {
                $content = str_replace($match[0], $globalVariables[$match[1]], $content);
            }
        }

        return $content;
    }
}
