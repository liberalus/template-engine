<?php

/**
 * @author  Evaldas Kazlauskas (kazlauskas.evaldas@gmail.com)
 *
 * @license http://www.gnu.org/licenses/old-licenses/gpl-2.0-standalone.html
 */

declare(strict_types = 1);

namespace Liberalu\Template\Parser;

/**
 * Class SCOPE
 */
class Scope
{
    public const VARIABLE = 'variable';

    public const BLOCK = 'block';
}
