<?php

/**
 * @author  Evaldas Kazlauskas (kazlauskas.evaldas@gmail.com)
 *
 * @license http://www.gnu.org/licenses/old-licenses/gpl-2.0-standalone.html
 */

declare(strict_types = 1);

namespace Liberalu\Template;

use Liberalu\Template\Core\Container;
use Liberalu\Template\Core\Filesystem;
use Liberalu\Template\Core\Parser;
use Symfony\Component\DependencyInjection\ContainerBuilder;

/**
 * Class Template
 */
class Template
{
    /** @var Filesystem */
    private $fileSystem;

    /** @var Parser */
    private $parser;

    /**
     *
     * @throws \Exception
     */
    public function __construct()
    {
        /** @var ContainerBuilder $container */
        $container = (new Container())->build();
        /** @var Filesystem filesystem */
        $filesytem = $container->get('core.file_system');
        $this->fileSystem = $filesytem;
        /** @var Parser $parser */
        $parser = $container->get('core.parser');
        $this->parser = $parser;
    }

    /**
     * @param string $filePath
     *
     * @param array  $variables
     *
     * @return string
     */
    public function output(string $filePath, array $variables = []): string
    {
        if (!$this->fileSystem->isFileValid($filePath)) {
            throw new \InvalidArgumentException('Template path is not valid');
        }

        $fileContent = $this->fileSystem->getContent($filePath);

        $parsedText = $this->parser->parse($fileContent, $variables);

        return $parsedText;
    }
}
