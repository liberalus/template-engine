<?php

declare(strict_types = 1);

namespace Liberalu\Template\Tests\Unit\Core;

use Liberalu\Template\Core\Parser;
use Liberalu\Template\Parser\Loop;
use PHPUnit\Framework\TestCase;

/**
 * Class LoopTest
 */
class LoopTest extends TestCase
{
    /** @var Loop */
    private $service;

    /** @var Parser */
    private $parserMock;

    /**
     * @throws \Exception
     */
    public function setUp()
    {
        $this->parserMock = $this->createMock(Parser::class);
        $this->service = new Loop($this->parserMock);
    }

    /**
     * @test
     */
    public function shouldParseLoop(): void
    {
        $this
            ->parserMock
            ->method('parse')
            ->willReturnCallback(function ($text) {
                if ('Name is {{name}}' === trim($text)) {
                    return 'Name is NAME';
                }

                return $text;
            });

        $block =
            '{{#each loop}}
                    Name is {{name}}
            {{/each}}';
        $parsedText = $this->service->parse($block, ['loop' => [['name' => 'NAME']]]);

        $this->assertSame('Name is NAME', $parsedText);
    }

    /**
     * @test
     */
    public function shouldThrowErrorIfVariableDoesntExist(): void
    {
        $this->expectException(\InvalidArgumentException::class);

        $block =
            '{{#each loop}}
                    Name is {{name}}
            {{/each}}';

        $this->service->parse($block);
    }

    /**
     * @test
     */
    public function shouldReturnLastCondition(): void
    {
        $this
            ->parserMock
            ->expects($this->at(0))
            ->method('parse')
            ->willReturnCallback(function ($text, $variables) {
                if (false === $variables['@last']) {
                    return 'OK';
                }

                return $text;
            });

        $this
            ->parserMock
            ->expects($this->at(1))
            ->method('parse')
            ->willReturnCallback(function ($text, $variables) {
                if (true === $variables['@last']) {
                    return 'OK';
                }

                return $text;
            });

        $block =
            '{{#each loop}}
                    Name is {{name}}
            {{/each}}';

        $this->service->parse($block,
            ['loop' =>
                [
                    ['name' => 'NAME'],
                    ['name' => 'NAME2'],
                ],
            ]
        );
    }
}
