<?php

declare(strict_types = 1);

namespace Liberalu\Template\Tests\Unit\Core;

use Liberalu\Template\Core\Parser;
use Liberalu\Template\Parser\Condition;
use PHPUnit\Framework\TestCase;

/**
 * Class ConditionTest
 */
class ConditionTest extends TestCase
{
    /** @var Condition */
    private $service;

    /** @var Parser */
    private $parserMock;

    /**
     * @throws \Exception
     */
    public function setUp()
    {
        $this->parserMock = $this->createMock(Parser::class);
        $this->service = new Condition($this->parserMock);
    }

    /**
     * @test
     */
    public function shouldParseCondition(): void
    {
        $this
            ->parserMock
            ->method('parse')
            ->willReturnArgument(0);

        $block ='{{#unless last}},{{else}}!{{/unless}}';

        $parsedText = $this->service->parse($block, ['last' => true]);
        $this->assertSame('!', $parsedText);

        $parsedText = $this->service->parse($block, ['last' => false]);
        $this->assertSame(',', $parsedText);
    }

    /**
     * @test
     */
    public function shouldThrowErrorIfVariableDoesnotExist(): void
    {
        $this->expectException(\InvalidArgumentException::class);

        $block ='{{#unless last}},{{else}}!{{/unless}}';

        $this->service->parse($block);
    }
}
