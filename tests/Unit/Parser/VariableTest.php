<?php

declare(strict_types = 1);

namespace Liberalu\Template\Tests\Unit\Core;

use Liberalu\Template\Parser\Variable;
use PHPUnit\Framework\TestCase;

/**
 * Class VariableTest
 */
class VariableTest extends TestCase
{
    /** @var Variable */
    private $service;

    /**
     * @throws \Exception
     */
    public function setUp()
    {
        $this->service = new Variable();
    }

    /**
     * @test
     */
    public function shouldParseVariable(): void
    {
        $block = 'Name is {{name}}';

        $parsedText = $this->service->parse($block, ['name' => 'NAME']);

        $this->assertSame('Name is NAME', $parsedText);
    }
}
