<?php

declare(strict_types = 1);

namespace Liberalu\Template\Tests\Unit\Core;

use Liberalu\Template\Parser\ParserInterface;
use Liberalu\Template\Parser\Scope;

/**
 * Class FakeParser
 */
class FakeParser implements ParserInterface
{
    /**
     * @return string
     */
    public function getName(): string
    {
        return 'fakeParser';
    }

    /**
     * @return string
     */
    public function getScope(): string
    {
        return Scope::VARIABLE;
    }

    /**
     * @param string $content
     * @param array  $globalVariables
     *
     * @return string
     */
    public function parse(string $content, array $globalVariables = []): string
    {
        return $content;
    }
}
