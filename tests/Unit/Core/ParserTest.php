<?php

declare(strict_types = 1);

namespace Liberalu\Template\Tests\Unit\Core;

use Liberalu\Template\Core\Parser;
use PHPUnit\Framework\TestCase;

/**
 * Class ParserTest
 */
class ParserTest extends TestCase
{
    /** @var Parser */
    private $service;

    /**
     * @throws \Exception
     */
    public function setUp()
    {
        $this->service = new Parser();
    }

    /**
     * @test
     */
    public function shouldAddParser(): void
    {
        $this->service->addParser(new FakeParser());

        $this->assertTrue(true);
    }

    /**
     * @test
     */
    public function shouldParseText(): void
    {
        $this->service->addParser(new FakeParser());

        $this->assertSame('any text', $this->service->parse('any text'));
    }
}
