<?php

declare(strict_types = 1);

namespace Liberalu\Template\Tests\Functional;

use Liberalu\Template\Template;
use PHPUnit\Framework\TestCase;

/**
 * Class Parser
 */
class ParseTemplateTest extends TestCase
{
    /** @var Template */
    private $service;

    /**
     * @throws \Exception
     */
    public function setUp()
    {
        $this->service = new Template();
    }

    /**
     * @test
     */
    public function shouldParseTemplate(): void
    {
        $expectedText = 'Hey Your name goes here, here\'s a poem for you:

  roses are red
  violets are blue
  you are able to solve this
  we are interested in you

';
        $name = 'Your name goes here';
        $stuff = [
            [
                'Thing' => "roses",
                'Desc'  => "red",
            ],
            [
                'Thing' => "violets",
                'Desc'  => "blue",
            ],
            [
                'Thing'  => "you",
                'Desc'  => "able to solve this"
            ],
            [
                'Thing'  => "we",
                'Desc'  => "interested in you",
            ],
        ];
        $output = $this->service->output(__DIR__.DIRECTORY_SEPARATOR.'template.tmpl', [
            'Name' => $name,
            'Stuff' => $stuff,
        ]);

        $this->assertSame($expectedText, $output);
    }


    /**
     * @test
     */
    public function shouldParseExtendedTemplate(): void
    {
        $expectedText = 'Hey Your name goes here, here\'s a slightly better formatted poem for you:

  roses are red,
  violets are blue,
  you are able to solve this,
  we are interested in you!

';
        $name = 'Your name goes here';
        $stuff = [
            [
                'Thing' => "roses",
                'Desc'  => "red",
            ],
            [
                'Thing' => "violets",
                'Desc'  => "blue",
            ],
            [
                'Thing'  => "you",
                'Desc'  => "able to solve this"
            ],
            [
                'Thing'  => "we",
                'Desc'  => "interested in you",
            ],
        ];
        $output = $this->service->output(__DIR__.DIRECTORY_SEPARATOR.'extra.tmpl', [
            'Name' => $name,
            'Stuff' => $stuff,
        ]);

        $this->assertSame($expectedText, $output);
    }

    /**
     * @test
     */
    public function shouldThrowErrorIfFileDoesnotExist(): void
    {
        $this->expectException(\InvalidArgumentException::class);

        $this->service->output('aaa.tmpl');
    }
}
